package com.xsis.Tugas1.models;

import javax.persistence.*;

@Entity
@Table(name = "karyawan")
public class Karyawan extends Common {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    @Column(name ="id")
    private Long id;

    @Column(name = "nama")
    private String nama;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToOne
    @JoinColumn(name = "nama_id", insertable = false, updatable = false)
    public JenisKaryawan namaKaryawan;

    @Column(name = "nama_id", nullable = false)
    private String namaId;

    public Long getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public JenisKaryawan getNamaKaryawan() {
        return namaKaryawan;
    }

    public void setNamaKaryawan(JenisKaryawan namaKaryawan) {
        this.namaKaryawan = namaKaryawan;
    }

    public String getNamaId() {
        return namaId;
    }

    public void setNamaId(String namaId) {
        this.namaId = namaId;
    }



    

   
}
