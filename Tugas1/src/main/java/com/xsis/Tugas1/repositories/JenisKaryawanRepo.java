package com.xsis.Tugas1.repositories;

import com.xsis.Tugas1.models.JenisKaryawan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JenisKaryawanRepo extends JpaRepository<JenisKaryawan, Long> {
}
