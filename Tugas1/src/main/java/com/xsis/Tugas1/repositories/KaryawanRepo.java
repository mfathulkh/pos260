package com.xsis.Tugas1.repositories;

import com.xsis.Tugas1.models.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KaryawanRepo extends JpaRepository<Karyawan, Long> {
}
